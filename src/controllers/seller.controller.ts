import { getRepository } from "typeorm";
import { Response, Request } from "express";

import { Seller } from "../entities/Seller";

export const getSellers = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Seller).find({ order: { name: "ASC" } });
  return res.json(results);
};

export const getSeller = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Seller).findOne(req.params.id, {
    order: { name: "ASC" },
  });
  return res.json(results);
};

export const createSeller = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const seller = await getRepository(Seller).create(req.body);
  const results = await getRepository(Seller).save(seller);
  return res.json(results);
};

export const updateSeller = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const seller = await getRepository(Seller).findOne(req.params.id);
  if (seller) {
    getRepository(Seller).merge(seller, req.body);
    const results = await getRepository(Seller).save(seller);
    return res.json(results);
  }
  return res.json({ msg: "Not found" });
};

export const deleteSeller = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Seller).delete(req.params.id);
  return res.json(results);
};
