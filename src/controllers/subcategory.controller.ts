import { getRepository } from "typeorm";
import { Response, Request } from "express";

import { Subcategory } from "../entities/Subcategory";
import { Category } from "../entities/Category";

export const getSubcategories = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Subcategory).find({
    order: { name: "ASC" },
  });
  return res.json(results);
};

export const getSubcategoriesWithCategory = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Subcategory).find({
    relations: ["category"],
    order: { name: "ASC" },
  });
  return res.json(results);
};

export const getSubcategoriesWithItems = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Subcategory).find({
    relations: ["items"],
    order: { name: "ASC" },
  });
  return res.json(results);
};

export const getSubcategory = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Subcategory).findOne(req.params.id, {
    order: { name: "ASC" },
  });
  return res.json(results);
};

export const getSubcategoryWithItems = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Subcategory).findOne(req.params.id, {
    relations: ["items"],
    order: { name: "ASC" },
  });
  return res.json(results);
};

export const createSubcategory = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const subcategory = await getRepository(Subcategory).create(req.body);
  const results = await getRepository(Subcategory).save(subcategory);
  return res.json(results);
};

export const updateSubcategory = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const subcategory = await getRepository(Subcategory).findOne(req.params.id);
  if (subcategory) {
    getRepository(Subcategory).merge(subcategory, req.body);
    const results = await getRepository(Subcategory).save(subcategory);
    return res.json(results);
  }
  return res.json({ msg: "Not found" });
};

export const deleteSubcategory = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Subcategory).delete(req.params.id);
  return res.json(results);
};
