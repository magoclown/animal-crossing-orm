import { getRepository } from "typeorm";
import { Response, Request } from "express";

import { Category } from "../entities/Category";

export const getCategories = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Category).find();
  return res.json(results);
};

export const getCategoriesWithSubcategories = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Category).find({
    relations: ["subcategories"],
  });
  return res.json(results);
};

export const getCategory = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Category).findOne(req.params.id);
  return res.json(results);
};

export const createCategory = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const category = await getRepository(Category).create(req.body);
  const results = await getRepository(Category).save(category);
  return res.json(results);
};

export const updateCategory = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const category = await getRepository(Category).findOne(req.params.id);
  if (category) {
    getRepository(Category).merge(category, req.body);
    const results = await getRepository(Category).save(category);
    return res.json(results);
  }
  return res.json({ msg: "Not found" });
};

export const deleteCategory = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Category).delete(req.params.id);
  return res.json(results);
};
