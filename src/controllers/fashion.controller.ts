import { getRepository } from "typeorm";
import { Response, Request } from "express";

import { Fashion } from "../entities/Fashion";

export const getFashions = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Fashion).find({
    order: { name: "ASC" },
  });
  return res.json(results);
};

export const getFashion = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Fashion).findOne(req.params.id, {
    order: { name: "ASC" },
  });
  return res.json(results);
};

export const createFashion = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const fashion = await getRepository(Fashion).create(req.body);
  const results = await getRepository(Fashion).save(fashion);
  return res.json(results);
};

export const updateFashion = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const fashion = await getRepository(Fashion).findOne(req.params.id);
  if (fashion) {
    getRepository(Fashion).merge(fashion, req.body);
    const results = await getRepository(Fashion).save(fashion);
    return res.json(results);
  }
  return res.json({ msg: "Not found" });
};

export const deleteFashion = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Fashion).delete(req.params.id);
  return res.json(results);
};
