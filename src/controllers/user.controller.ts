import { getRepository } from "typeorm";
import { Response, Request } from "express";

import { User } from "../entities/User";

export const getUsers = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(User).find({ order: { name: "ASC" } });
  return res.json(results);
};

export const login = async (req: Request, res: Response): Promise<Response> => {
  const results = await getRepository(User)
    .createQueryBuilder("user")
    .select(["user.id","user.friendCode","user.island","user.name","user.character","user.discord"])
    .leftJoinAndSelect("user.catalogs", "catalog")
    .where("user.name = :name", { name: req.body.name })
    .orWhere("user.email = :email", { email: req.body.email })
    .andWhere("user.password LIKE :password", { password: `${req.body.password}` })
    .getOne();
  return res.json(results);
};

export const getUser = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(User).findOne(req.params.id, {
    order: { name: "ASC" },
  });
  return res.json(results);
};

export const createUser = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const user = await getRepository(User).create(req.body);
  const results = await getRepository(User).save(user);
  return res.json(results);
};

export const updateUser = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const user = await getRepository(User).findOne(req.params.id);
  if (user) {
    getRepository(User).merge(user, req.body);
    const results = await getRepository(User).save(user);
    return res.json(results);
  }
  return res.json({ msg: "Not found" });
};

export const deleteUser = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(User).delete(req.params.id);
  return res.json(results);
};
