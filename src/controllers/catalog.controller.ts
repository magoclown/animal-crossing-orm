import { getRepository } from "typeorm";
import { Response, Request } from "express";

import { Catalog } from "../entities/Catalog";
import { User } from "../entities/User";
import { Item } from "../entities/Item";

export const getCatalogs = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Catalog).find({
    relations: ["items", "user"],
    order: { name: "ASC" },
  });
  return res.json(results);
};

export const getCatalog = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Catalog)
    .createQueryBuilder("catalog")
    .leftJoinAndSelect("catalog.items", "item")
    .leftJoinAndSelect("catalog.user", "user")
    .leftJoinAndSelect("item.subcategory", "subcategory")
    .leftJoinAndSelect("item.seller", "seller")
    .leftJoinAndSelect("item.fashion", "fashion")
    .where('catalog.id = :id', {id: req.params.id})
    .getOne();
  return res.json(results);
};

export const getCatalogByUserID = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const user = new User();
  user.id = parseInt(req.params.id);
  const results = await getRepository(Catalog).find({
    relations: ["items"],
    order: { name: "ASC" },
    where: { user: user },
  });
  return res.json(results);
};

export const createCatalog = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const catalog = await getRepository(Catalog).create(req.body);
  const results = await getRepository(Catalog).save(catalog);
  return res.json(results);
};

export const addItemToCatalog = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const catalog = await getRepository(Catalog).findOne(req.params.id, {
    relations: ["items"],
  });
  const catalogOriginal = catalog;
  const item = await getRepository(Item).findOne(req.body);
  let results;
  let exist;
  if (catalog && item && catalogOriginal) {
    exist = catalog.items.find((x) => x.id == item.id);
    if (!exist) {
      catalog.items.push(item);
    }
    getRepository(Catalog).merge(catalogOriginal, catalog);
    results = await getRepository(Catalog).save(catalog);
  }

  return res.json(results);
};

export const updateCatalog = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const catalog = await getRepository(Catalog).findOne(req.params.id);
  if (catalog) {
    getRepository(Catalog).merge(catalog, req.body);
    const results = await getRepository(Catalog).save(catalog);
    return res.json(results);
  }
  return res.json({ msg: "Not found" });
};

export const removeItemToCatalog = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const catalog = await getRepository(Catalog).findOne(req.params.id, {
    relations: ["items"],
  });
  const catalogOriginal = catalog;
  const headerItem = req.header("item");
  const item = headerItem
    ? await getRepository(Item).findOne(parseInt(headerItem))
    : 0;
  let results;
  let exist;
  if (catalog && item && catalogOriginal) {
    exist = catalog.items.filter((x) => x.id != item.id);
    catalog.items = exist;
    getRepository(Catalog).merge(catalogOriginal, catalog);
    results = await getRepository(Catalog).save(catalog);
  }

  return res.json(results);
};

export const deleteCatalog = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Catalog).delete(req.params.id);
  return res.json(results);
};
