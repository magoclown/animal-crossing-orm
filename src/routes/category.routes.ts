import { Router } from "express";

import {
  getCategories,
  getCategory,
  createCategory,
  updateCategory,
  deleteCategory,
  getCategoriesWithSubcategories,
} from "../controllers/category.controller";

const router = Router();

router.get("/categories", getCategories);
router.get("/categories/ws", getCategoriesWithSubcategories);
router.get("/categories/:id", getCategory);
router.post("/categories", createCategory);
router.put("/categories/:id", updateCategory);
// router.delete('categories/:id', deleteCategory);

export default router;
