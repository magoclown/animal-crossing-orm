import { Router } from "express";

import {
  getFashions,
  getFashion,
  createFashion,
  updateFashion,
} from "../controllers/fashion.controller";

const router = Router();

router.get("/fashions", getFashions);
router.get("/fashions/:id", getFashion);
router.post("/fashions", createFashion);
router.put("/fashions/:id", updateFashion);
// router.delete('fashions/:id', deleteFashion);

export default router;
