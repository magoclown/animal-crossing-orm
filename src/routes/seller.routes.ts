import { Router } from "express";

import {
  getSeller,
  getSellers,
  createSeller,
  updateSeller,
} from "../controllers/seller.controller";

const router = Router();

router.get("/sellers", getSellers);
router.get("/sellers/:id", getSeller);
router.post("/sellers", createSeller);
router.put("/sellers/:id", updateSeller);
// router.delete('sellers/:id', deleteSeller);

export default router;
