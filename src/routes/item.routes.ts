import { Router } from "express";

import {
  getItems,
  getItem,
  createItem,
  updateItem,
  deleteItem,
  getItemsWithAll,
  getItemsWithAllBySubcategoryId,
  getItemWithAll,
  getItemsWithAllByCategoryId,
  getItemsWithAllBySubcategoryIdAndName
} from "../controllers/item.controller";

const router = Router();

router.get("/items", getItems);
router.get("/items/wa", getItemsWithAll);
router.get("/items/wa/sub/:id", getItemsWithAllBySubcategoryId);
router.get("/items/wa/cat/:id", getItemsWithAllByCategoryId);
router.get("/items/wa/sub/:id/:name", getItemsWithAllBySubcategoryIdAndName);
router.get("/items/:id", getItem);
router.get("/items/wa/:id", getItemWithAll);
router.post("/items", createItem);
router.put("/items/:id", updateItem);
// router.delete('items/:id', deleteItem);

export default router;
