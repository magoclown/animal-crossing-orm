import { Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToMany } from "typeorm";

import { Subcategory } from "./Subcategory";
import { Item } from "./Item";

@Entity({ name: "category" })
export class Category {
  @PrimaryGeneratedColumn({ name: "category_id" })
  id: number;
  @Column({ name: "category_name" })
  name: string;
  @OneToMany((type) => Subcategory, (subcategories) => subcategories.category)
  subcategories: Subcategory[];
}
