import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";

import { Item } from "./Item";

@Entity({name: 'seller'})
export class Seller {
    @PrimaryGeneratedColumn({name: 'seller_id'})
    id: number;
    @Column({name: 'seller_name'})
    name: string;
    @OneToMany(type => Item, items => items.seller)
    items: Item[];
}