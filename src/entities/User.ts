import { PrimaryGeneratedColumn, Column, OneToMany, Entity } from "typeorm";
import { Catalog } from "./Catalog";

@Entity({ name: "user" })
export class User {
  @PrimaryGeneratedColumn({ name: "user_id" })
  id: number;
  @Column({ name: "user_friend_code" })
  friendCode: string;
  @Column({ name: "user_name" })
  name: string;
  @Column({ name: "user_island" })
  island: string;
  @Column({ name: "user_character" })
  character: string;
  @Column({ name: "user_email" })
  email: string;
  @Column({ name: "user_password", select: false })
  password: string;
  @Column({ name: "user_discord" })
  discord: string;
  @OneToMany((type) => Catalog, (catalog) => catalog.user)
  catalogs: Catalog[];
}
