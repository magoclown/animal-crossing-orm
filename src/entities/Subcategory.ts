import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from "typeorm";

import { Category } from "./Category";
import { Item } from "./Item";

@Entity({name: 'subcategory'})
export class Subcategory {
    @PrimaryGeneratedColumn({name: 'subcategory_id'})
    id: number;
    @Column({name: 'subcategory_name'})
    name: string;
    @ManyToOne(type => Category, category => category.subcategories)
    category: Category;
    @OneToMany(type => Item, items => items.subcategory)
    items: Item[];
}